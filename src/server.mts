/**
 * SPDX-PackageName: kwaeri/server
 * SPDX-PackageVersion: 0.2.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as http from 'http';
import * as qs from 'querystring';
import * as url from 'url';
import { Console } from '@kwaeri/console';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:server' ),
      output = new Console( { color: false, background: false, decor: [] } );


export class Server {
    /**
     * @var { Configuration } configuration
     */
    private configuration;


    /**
     *
     * @var { Router } router
     */
    private router;


    /**
     * @var { Route } route
     */
    protected route;


    /**
     * Class constructor
     */
    constructor( configuration: any ) {
        // Store the configuration:
        this.configuration = configuration;

        // Create an instance of the router:
        this.router = new this.configuration.router( this.configuration );

        // Bind the route function to an instance of our router's:
        this.route = this.router.route;
        this.route.bind( this.router );
    }


    /**
     * Starts the server
     *
     * @param { void }
     *
     * @returns { void }
     */
    start() {
        // Store a reference to this instance of the server:
        const server = this;

        // Define our http server application:
        const app = http.createServer(
            ( request: http.IncomingMessage, response: http.ServerResponse<http.IncomingMessage> ) => {
                // Pass the request to the error handler:
                DEBUG( `Pass request to the error handler` );
                server.processError( request, response );

                // Next, let's monkey patch some convenience properties into our
                // request object:
                const parsed = url.parse( request.url as string, true );
                ( request as any ).path = parsed.pathname;
                ( request as any ).queryString = parsed.search;
                ( request as any ).query = parsed.query;

                // Continuing - if we are in development mode we should log any action
                // to the console:
                DEBUG( `%s %s`, request.method, ( request as any ).path );

                // Finally, determine how to handle the request based on the method,
                // or HTTP action/verb:
                switch( request.method ) {
                    case 'GET':
                        server.route( request, response );
                        break;
                    case 'POST':
                        server.processPost( request, response );
                        break;
                    case 'PUT':
                        server.processPost( request, response );
                        break;
                    default:
                        server.route( request, response );
                        break;
                }
            }
        );

        // Start the http server
        app.listen( this.configuration.server.port[this.configuration.appType], this.configuration.server.host );

        // Fancy server start output:
        console.log(
            output.color( `blue` ).decor( ['bright'] ).buffer( `Platform: ` )
            .normalize().buffer( `${process.platform}` ).dump()
        );
        console.log(
            output.color( `blue` ).decor( ['bright'] ).buffer( `Architecture: ` )
            .normalize().buffer( `${process.arch}` ).dump()
        );
        console.log(
            output.color( 'blue' ).decor( ['bright'] )
            .buffer( `Listening to http://${this.configuration.server.host} on port ${this.configuration.server.port[this.configuration.appType]}`)
            .dump()
        );
    };


    /**
     * Processes server error(s)
     *
     * @param { http.IncomingMessage } request
     * @param { http.ServerResponse<http.IncomingMessage> } response
     *
     * @returns { void }
     */
    processError( request: http.IncomingMessage, response: http.ServerResponse<http.IncomingMessage> ): void {
        // Let's just do some basic loggned
        request.on(
            'error',
            error => {
                // Fancy error output:
                console.log(
                    output.color( `white` ).decor( ['bright'] ).buffer( `Error: '` )
                    .normalize().color( 'red' ).decor( ['bright'] ).buffer( `${error.message}` )
                    .normalize().color( 'white' ).decor( ['bright'] ).buffer( `' ` ).dump()
                );
                console.log(
                    output.color( `white` ).decor( ['bright'] ).buffer( `[STACK]: '` )
                    .normalize().color( 'red' ).decor( ['bright'] ).buffer( `${error.stack}` )
                    .normalize().color( 'white' ).decor( ['bright'] ).buffer( `' ` ).dump()
                );
                //console.error( error.stack );
            }
        );
    }


    /**
     * Processes POST/PUT request
     *
     * @param { http.IncomingMessage } request
     * @param { http.ServerResponse<http.IncomingMessage> } response
     *
     * @returns { void }
     */
    processPost( request: http.IncomingMessage, response: http.ServerResponse<http.IncomingMessage> ): void {
        const server = this;

        // Extract any posted data
        let posted = '';
        request.on(
            'data',
            data  => {
                posted += data.toString();

                // Make sure we aren't getting DOS bombed
                if( posted.length > (1e6*5) ) { // Assuming this works out to 500.1.0 or ~ 5MB
                    console.log( `Network flooded. '${request.method}' size: ${posted.length}. Connection destroyed` );

                    request.socket.destroy()
                }
            }
        );

        request.on(
            'end',
            () => {
                // Add a new member to our request object containing the data. We
                // have to be careful how we handle this:
                let value: any = null;

                switch( request.headers['content-type'] ) {
                    // For JSON content-type we simply JSON.parse it:
                    case 'application/json':
                    case 'text/json':
                        value = JSON.parse( posted );
                        break;
                    // For x-www-form-urlencoded querystring we use querystring to parse it:
                    case 'application/x-www-form-urlencoded':
                        value = qs.parse( posted );
                        break;
                    // For multipart form-data we do not yet have this impelmented, as
                    // this can be inclusive of sending full file contents to the server:
                    // In lieu of this, we'll follow suite with text/plain and forward the
                    // raw data (for now):
                    case 'multipart/form-data':
                        console.log( 'multipart/form-data content-type parsing not yet implemented; packing raw data...' );
                        value = posted;
                        break;
                    // For default we will assume a fallback of text/plain:
                    case 'text/plain':
                    default:
                        value = posted;
                        break;
                }

                // Create a property attributes object:
                //const attributes = { value, writable: false };

                // Define a new property on the ServerReponse object:
                //Object.defineProperty( request, "posted", attributes )
                ( request as any )["posted"] = value;

                // Route the request
                server.route( request, response );
            }
        );

        request.on(
            'error',
            error => {
                // Fancy error output:
                console.log(
                    output.color( `white` ).decor( ['bright'] ).buffer( `[` )
                    .color( 'red' ).buffer( `PROCESS POST` )
                    .color( 'white' ).buffer( `] Error receiving http body: '` )
                    .normalize().color( 'red' ).decor( ['bright'] ).buffer( `${error.message}` )
                    .normalize().color( 'white' ).decor( ['bright'] ).buffer( `' ` ).dump()
                );

                console.log( `[PROCESS POST ERROR] Error receiving http body: '${error.message}' [STACK]: ${error.stack}` );
                //console.log( `Error receiving http body [${request.method}]: ${error.message} line: ${/\(file:[\w\d/.-]+:([\d]+)/.exec( error.stack )[1]}` );
            }
        );
    };
}
