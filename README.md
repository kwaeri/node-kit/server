# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-server [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The @kwaeri/server component for the @kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/node-kit/server/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/server/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/server/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/server/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

kwaeri/server reinvents and modernizes the server portion of the node-kit application platform. It expects to be fed a [configuration](https://gitlab.com/mmod/kwaeri-platform/blob/master/config.js) (specifically, the contents of the development or production properties), one that is in the root of a project which makes use of kwaeri/server.

The server component also requires that several other required components be bundled with the configuration, prefereably through a project's initialization process, such as:

* A Router component
* A Session component
  * Requiring a compatible Session Store
* A Controller component
* A Model component
* A Renderer component
* A Utility component

As the server component was originally baked into the nk module, its usage was entirely controlled by it. As we discern the process for decoupling the individual components which make up a kwaeri application, we'll begin to simplify the act of doing so, and provide documentation for utilizing each component individually.

## Getting Started

**NOTE**

This module is not ready for production, but published for testing and development purposes. It is in a beta state that is intended for allowing a larger audience of users to try out anything that may already be available, but please be aware that there is likely many aspects of the platform which are not working and/or completely broken; This is intended to allow users to find and report such issues so that they may be fixed. Updated documentation and complete examples and tutorials for getting started will be provided as the platform rewrite nears completion.

### Installation

[kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope and provides a single point of entry for both the kwaeri/node-kit application framework and kwaeri/cli component of the kwaeri platform.

[kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the various user-executable framework components under the kwaeri scope, and provides a single point of entry to the CLI tooling of the kwaeri platform.

However, if you wish to install kwaeri/server and utilize it specifically - perform the following steps to get started:

Install @kwaeri/server:

```bash
npm install @kwaeri/server
```

### Usage

Basic usage of the server requires nothing more than to import it:

```typescript
import { Server } from '@kwaeri/server';
```

However, the server component is intended to be used in conjunction with several other components which make up the node-kit framework as part of the kwaeri application platform. This is done officially via the [kwaeri/node-kit](https://www.gitlab.com/kwaeri/node-kit/node-kit) entry point.

To user the server component directly, you'll need to first include the component, and follow up with providing its complimenting components as part of its configuration. Here are the imports used by node-kit to satisfy the required component facilities:

```typescript
// INCLUDES
import { Server } from '@kwaeri/server';
import { Router } from '@kwaeri/router';
import { Session } from '@kwaeri/session';
import { Controller } from '@kwaeri/controller';
import { Model } from '@kwaeri/model';
import { Driver } from '@kwaeri/driver';
import { Renderer } from '@kwaeri/renderer';
import { Utility } from '@kwaeri/utility';

// npm install --save sjcl,
import * as sjcl from 'sjcl';

// npm install --save-dev @kwaeri/md5-js
import { crypt_md5 } from '@kwaeri/md5-js';

// npm install --save @kwaeri/developer-tools
import { kdt } from '@kwaeri/node-kit/node-kit/core/kdt';

import debug from 'debug';

// DEFINES
const _ = new kdt()
      DEBUG = debug( 'nodekit:server' ),;
```

You'll need to prepare a configuration for the server component. In the node-kit entry, this is done through a standardized configuration file found in an application's `conf` directory.

You can use [this configuration](https://gitlab.com/mmod/kwaeri-platform/blob/master/config.js) as an example. Please note that you'll only need the contents of the development or production properties found in the example. Furthermore, you do not need to have paths for both admin and app, the paths could be nested directly within the root of the object itself. You are free to structure your application as you wish, provided you update the configuration to reflect it properly, and structure the resulting configuration object appropriately.

You may also enhance this entry point so that it supports a full configuration such as that demonstrated in the official entry point.

After reading the configuration, you'll need to prepare (instantiate) each of the components, and bundle them into the configuration so that the application we're building with the server component has the facilities it needs for handling all of its processes. This process includes instantiating the many components, providing them their configurations, and ultimately preparing everything from paths, to a session provider and its store, data persistence, etc.

Next, it's time to build the server component:

```javascript
...
const server = new Server( configuration );
```

Finally, you can start the server:

```javascript
server.start();
```

Of course, you'll need to provide the application structure to support the now running application, and make use of its dynamic routing capabilities. Documentation specific to this will be included at some point in the future. However, and again - [kwaeri/node-kit](https://gitlab.com/kwaeri/node-kit/node-kit) serves as a perfect example of this.

More to come!

***NOTE:***

This information will be revisited as the platform nears an official initial release.

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:contact-project+kwaeri-node-kit-server-8006479-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
